package com.wildadventures.microservice.users.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * This is the CityFr model
 * @author amarjane
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CityFr {
    private Integer id;
    private DepartmentFr departmentFr;
    private String nom;
    private String codepostal;
    private Float longitude;
    private Float latitude;
}

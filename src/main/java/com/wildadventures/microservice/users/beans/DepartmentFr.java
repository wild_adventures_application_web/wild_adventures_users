package com.wildadventures.microservice.users.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * This is the DepartmentFr model
 * @author amarjane
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DepartmentFr {
    private String id;
    private RegionFr regionFr;
    private String nom;
}

package com.wildadventures.microservice.users.beans;


import com.wildadventures.microservice.users.enumeration.ProfilEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * This is the UserDetailsG model
 * @author amarjane
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserDetailsG {

    private String mail;
    private String password;
    private ProfilEnum profil;
}

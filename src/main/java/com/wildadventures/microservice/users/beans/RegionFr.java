package com.wildadventures.microservice.users.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * This is the RegionFr model and it is related to the regions table
 * @author amarjane
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RegionFr {
    private Integer id;
    private String nom;
}

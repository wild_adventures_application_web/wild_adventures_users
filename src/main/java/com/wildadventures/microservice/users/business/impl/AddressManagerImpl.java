package com.wildadventures.microservice.users.business.impl;

import com.wildadventures.microservice.users.business.contract.AddressManager;
import com.wildadventures.microservice.users.consumer.dao.AddressDao;
import com.wildadventures.microservice.users.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * AddressManagerImpl Implementation consume methods into interface AddressDao
 * @see AddressDao
 * @author amarjane
 * @version 1.0
 */
@Component
public class AddressManagerImpl implements AddressManager {

    @Autowired
    private AddressDao addressDao;

    private Address address;
    private List<Address> addressList;

    /**
     * Create new Address type Object
     * @param address
     * @return Address
     **/
    @Override
    public Address createAddress(Address address){
        return addressDao.save(address);
    }

    /**
     * Search Address with user type Object
     * @param id
     * @return List<Address>
     **/
    @Override
    public List<Address> findAddressByUser(Long id){
        addressList = addressDao.findAddressById_user(id);
        return addressList;
    }

    /**
     * Search Address with id type Long
     * @param id
     * @return Address
     **/
    @Override
    public Address findAddressById(Long id){
        if (id>0){
            address = addressDao.findAddressById_address(id);
            return address;
        }else {
            return null;
        }
    }

    /**
     * Search Address with name type String
     * @param id,name
     * @return Address
     **/
    @Override
    public Address findAddressByNameAndId(Long id, String name){
        if (id>0 && !name.isEmpty()){
            address = addressDao.findAddressByNameAAndId_address(id,name);
            return address;
        }else {
            return null;
        }
    }

    /**
     * Upadate Address with Address type Object
     * @param address
     * @return Address
     **/
    @Override
    public Address updateAddress(Address address){
        address = addressDao.save(address);
        return address;
    }

    /**
     * Delete Address with id type Long
     * @param id
     * @return boolean
     **/
    @Override
    public boolean deleteAddressById(Long id){
        addressDao.deleteAddressById_address(id);
        return addressDao.findAddressById_address(id) != null;
    }

    /**
     * Delete Address with id user type Long
     * @param id
     * @return void
     **/
    @Override
    public void deleteAddressByIdUser(Long id){
        addressDao.deleteAddressById_user(id);
    }

}

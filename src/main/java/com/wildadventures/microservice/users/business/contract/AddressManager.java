package com.wildadventures.microservice.users.business.contract;

import com.wildadventures.microservice.users.model.Address;
import com.wildadventures.microservice.users.model.User;

import java.util.List;
import java.util.Optional;

/**
 * AddressManager interface describing AddressManagerImpl behavior
 * @see com.wildadventures.microservice.users.business.impl.UserManagerImpl
 * @author amarjane
 * @version 1.0
 */
public interface AddressManager {

    /**
     * Create new Address type Object
     * @param address
     * @return Address
     **/
    Address createAddress(Address address);

    /**
     * Search Address with user type Object
     * @param id
     * @return List<Address>
     **/
    List<Address> findAddressByUser(Long id);

    /**
     * Search Address with id type Long
     * @param id
     * @return Address
     **/
    Address findAddressById(Long id);

    /**
     * Search Address with name type String
     * @param id,name
     * @return Address
     **/
    Address findAddressByNameAndId(Long id, String name);

    /**
     * Upadate Address with Address type Object
     * @param address
     * @return Address
     **/
    Address updateAddress(Address address);

    /**
     * Delete Address with id type Long
     * @param id
     * @return boolean
     **/
    boolean deleteAddressById(Long id);

    /**
     * Delete Address with id user type Long
     * @param id
     * @return void
     **/
    void deleteAddressByIdUser(Long id);


}

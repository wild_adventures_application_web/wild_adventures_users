package com.wildadventures.microservice.users.business.impl;

import com.wildadventures.microservice.users.business.contract.UserManager;
import com.wildadventures.microservice.users.consumer.dao.AddressDao;
import com.wildadventures.microservice.users.consumer.dao.UserDao;
import com.wildadventures.microservice.users.enumeration.ProfilEnum;
import com.wildadventures.microservice.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * UserManagerImpl Implementation consume methods into interface UserDao
 * @see UserDao
 * @author amarjane
 * @version 1.0
 */
@Component
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDao userDao;

    @Autowired
    private AddressDao addressDao;

    private User user;

    /**
     * Create User with User type Object
     * @param user
     * @return User
     */
    @Override
    public User createUser(User user){
        user.setPassword(user.getPassword());
        return userDao.save(user);
    }

    /**
     * Search all User
     * @return List<User>
     */
    @Override
    public List<User> findAllUsers(){
        List<User> userList = userDao.findAll();
        return userList;
    }

    /**
     * Search User with id type Long
     * @param id
     * @return User
     */
    @Override
    public User findUserById(Long id){
        if (id>0) {
            user = userDao.findUserById_user(id);
            return user;
        }else {
            return null;
        }
    }

    /**
     * Search User with id type Long
     * @param mail
     * @return User
     */
    @Override
    public User findUserByMail(String mail){
        user = userDao.findUserByMail(mail);
        return user;
    }

    /**
     * Search all User with id_profil type Long
     * @param profilEnum
     * @return List<User>
     */
    @Override
    public List<User> findAllUsersByProfile(ProfilEnum profilEnum){
        List<User> userList = userDao.findUserByProfil(profilEnum);
        return userList;
    }

    /**
     * Search User with mail and password type String
     * @param mail
     * @param Password
     * @return Optional<User>
     */
    @Override
    public User findUserByMailAndPassword(String mail, String Password){
        user = userDao.findUserByMailAndPassword(mail, Password);
        return user;
    }

    /**
     * Update User with User type Object
     * @param user
     * @return User
     */
    @Override
    public User updateUser(User user){
        user = userDao.save(user);
        return user;
    }

    /**
     * Delete User with id type Long
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteUser(Long id){
        addressDao.deleteAddressById_user(id);
        userDao.deleteById(id);
        return userDao.findUserById_user(id) != null;
    }

    private static String bcryptMdp(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}

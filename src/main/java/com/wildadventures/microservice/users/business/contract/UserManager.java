package com.wildadventures.microservice.users.business.contract;

import com.wildadventures.microservice.users.enumeration.ProfilEnum;
import com.wildadventures.microservice.users.model.User;

import java.util.List;
import java.util.Optional;

/**
 * UserManager interface describing UserManagerImpl behavior
 * @see com.wildadventures.microservice.users.business.impl.UserManagerImpl
 * @author amarjane
 * @version 1.0
 */
public interface UserManager {

    /**
     * Create User with User type Object
     * @param user
     * @return User
     */
    User createUser(User user);

    /**
     * Search all User
     * @return List<User>
     */
    List<User> findAllUsers();

    /**
     * Search User with id type Long
     * @param id
     * @return User
     */
    User findUserById(Long id);

    /**
     * Search User with id type Long
     * @param mail
     * @return User
     */
     User findUserByMail(String mail);

    /**
     * Search all User with id_profil type Long
     * @param profilEnum
     * @return List<User>
     */
    List<User> findAllUsersByProfile(ProfilEnum profilEnum);

    /**
     * Search User with mail and password type String
     * @param mail
     * @param Password
     * @return User
     */
    User findUserByMailAndPassword(String mail, String Password);

    /**
     * Update User with User type Object
     * @param user
     * @return User
     */
    User updateUser(User user);

    /**
     * Delete User with id type Long
     * @param id
     * @return boolean
     */
    boolean deleteUser(Long id);
}

package com.wildadventures.microservice.users.consumer.dao;

import com.wildadventures.microservice.users.model.Address;
import com.wildadventures.microservice.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * AddressDao interface extends JPA to consume the Address table in the database
 * @see JpaRepository
 * @author amarjane
 * @version 1.0
 */
@Repository
@Transactional
public interface AddressDao extends JpaRepository<Address, Long>{

    /**
     * Search Address with id type Long
     * @param id_address
     * @return Address
     **/
    @Query("SELECT u FROM address u WHERE u.id_address =:id_address")
    Address findAddressById_address(
            @Param("id_address") Long id_address);

    /**
     * Search Address with user type Object
     * @param id_user
     * @return List<Address>
     **/
    @Query("SELECT u FROM address u WHERE u.id_user =:id_user")
    List<Address> findAddressById_user(
            @Param("id_user") Long id_user);

    /**
     * Search Address with name type String
     * @param id_address,name
     * @return Address
     **/
    @Query("SELECT u FROM address u WHERE u.id_address =:id_address AND name =:name")
    Address findAddressByNameAAndId_address(
            @Param("id_address") Long id_address,
            @Param("name") String name);

    /**
     * Delete Address with id type Long
     * @param id_address
     * @return void
     **/
    @Modifying
    @Transactional
    @Query("DELETE FROM address u WHERE u.id_address =:id_address")
    void deleteAddressById_address(
            @Param("id_address") Long id_address);

    /**
     * Delete Address with id type Long
     * @param id_user
     * @return void
     **/
    @Modifying
    @Transactional
    @Query("DELETE FROM address u WHERE u.id_user =:id_user")
    void deleteAddressById_user(
            @Param("id_user") Long id_user);

}

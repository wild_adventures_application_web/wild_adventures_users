package com.wildadventures.microservice.users.consumer.dao;

import com.wildadventures.microservice.users.enumeration.ProfilEnum;
import com.wildadventures.microservice.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * UserDao interface extends JPA to consume the Users table in the database
 * @see JpaRepository
 * @author amarjane
 * @version 1.0
 */
@Repository
@Transactional
public interface UserDao extends JpaRepository<User, Long>{

    /**
     * Search User
     * @param id_user
     * @return User
     **/
    @Query("SELECT u FROM users u WHERE u.id_user =:id_user")
    User findUserById_user(
            @Param("id_user") Long id_user);

    /**
     * Search User
     * @param mail
     * @return User
     **/
    @Query("SELECT u FROM users u WHERE u.mail =:mail")
    User findUserByMail(
            @Param("mail") String mail);

    /**
     * Search all User
     * @param profil
     * @return List<User>
     **/
    List<User> findUserByProfil(ProfilEnum profil);

    /**
     * Search User with mail and password type String
     * @param mail
     * @param Password
     * @return User
     **/
    User findUserByMailAndPassword(String mail, String Password);

}

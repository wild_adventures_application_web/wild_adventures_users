package com.wildadventures.microservice.users.enumeration;

/**
 * Labels Centralisation
 * @author amarjane
 * @version 1.0
 */
public enum Strings {

    CREATE_RESOURCE_HATEOAS("create resource hateoas for responseEntity"),

    SEARCH_ADDRESS_IN_PROGRESS("Search address in progress ..."),
    SEARCH_PROFIL_IN_PROGRESS("Search profil in progress ..."),
    SEARCH_USER_IN_PROGRESS("Search user in progress ..."),

    SUCCESS_CREATE_ADDRESS("Create address success"),
    SUCCESS_CREATE_PROFIL("Create profil success"),
    SUCCESS_CREATE_USER("Create user success"),
    SUCCESS_FIND_ADDRESS("Find address success"),
    SUCCESS_FIND_PROFIL("Find profil success"),
    SUCCESS_FIND_USER("Find user success"),
    SUCCESS_FIND_PROFILS("Find profils success"),
    SUCCESS_FIND_USERS("Find users success"),

    VERIFY_ADDRESS_IS_EXIST("Verify if address is exist"),
    VERIFY_PROFIL_IS_EXIST("Verify if profil is exist"),
    VERIFY_USER_IS_EXIST("Verify if user is exist"),

    VERIFY_ADDRESS_IS_NOT_EXIST("Verify if address is not exist"),
    VERIFY_PROFIL_IS_NOT_EXIST("Verify if profil is not exist"),
    VERIFY_USER_IS_NOT_EXIST("Verify if user is not exist"),

    VERIFY_BODY_ADDRESS_NOT_NULL_OR_EMPTY("Verify body address is not null or empty"),
    VERIFY_BODY_PROFIL_NOT_NULL_OR_EMPTY("Verify body profil is not null or empty"),
    VERIFY_BODY_USER_NOT_NULL_OR_EMPTY("Verify body user is not null or empty"),

    VERIFY_LIST_ALL_ADDRESS_NOT_NULL("Verify list find is not null"),
    VERIFY_LIST_ALL_PROFILS_NOT_NULL("Verify list find is not null"),
    VERIFY_LIST_ALL_USER_NOT_NULL("Verify list find is not null"),

    EXCEPTION_NO_CONTENT_BODY_ADDRESS("Body Address is not complete or it contains an error"),
    EXCEPTION_NO_CONTENT_BODY_PROFIL("Body Profil is not complete or it contains an error"),
    EXCEPTION_NO_CONTENT_BODY_USER("Body User is not complete or it contains an error"),

    EXCEPTION_NOT_FOUND_ALL_ADDRESS("Address not found"),
    EXCEPTION_NOT_FOUND_ALL_PROFILS("Profils not found"),
    EXCEPTION_NOT_FOUND_ALL_USERS("Users not found"),
    EXCEPTION_NOT_FOUND_ADDRESS("Address not found"),
    EXCEPTION_NOT_FOUND_PROFIL("Profil not found"),
    EXCEPTION_NOT_FOUND_USER("User not found"),

    EXCEPTION_READY_EXIST_ADDRESS("Address a ready exist!"),
    EXCEPTION_READY_EXIST_PROFIL("Profil a ready exist!"),
    EXCEPTION_READY_EXIST_USER("User a ready exist!"),

    EXCEPTION_UNABLE_TO_UPDATE_ADDRESS("Unable to update this address"),
    EXCEPTION_UNABLE_TO_UPDATE_PROFIL("Unable to update this profil"),
    EXCEPTION_UNABLE_TO_UPDATE_USER("Unable to update this user");

    private final String message;

    /**
     * @param message
     */
    Strings(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}

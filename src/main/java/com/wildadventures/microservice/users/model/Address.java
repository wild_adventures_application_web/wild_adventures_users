package com.wildadventures.microservice.users.model;

import com.fasterxml.jackson.annotation.*;
import com.wildadventures.microservice.users.beans.CityFr;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This is the Address model and it is related to the Address table
 * @author amarjane
 * @version 1.0
 */
//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonInclude(JsonInclude.Include.NON_NULL)
//@Accessors(chain = true)
//@RequiredArgsConstructor
//@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
//@ToString (exclude = "user")
@Entity(name = "address")
@Table(name = "address")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id_address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JoinColumn(name = "id_address", updatable=false, nullable=false)
    private long id_address;

    @NotNull
    //@JsonBackReference
    //@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    private Long id_user;

    //    @NotNull
    //    @ManyToOne(fetch = FetchType.EAGER)
    //    @Column(name = "id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    //    private User user;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "name")
    private String name;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "name_address")
    private String name_address;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "city")
    private String city;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "country")
    private String country;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "postalcode")
    private String postalcode;

    @NotNull
    @JoinColumn(name = "id_location")
    private Integer id_location;

    @Transient
    private CityFr cityFr;

/*    @NotNull
    @Column(name = "id_user")
    private long idUser;*/

    @Size(min = 1, max = 50)
    @JoinColumn(name = "stage")
    private String stage;

    @Size(min = 1, max = 50)
    @JoinColumn(name = "door")
    private String door;

    @Size(min = 1, max = 50)
    @JoinColumn(name = "portal_code")
    private String portalCode;
}

package com.wildadventures.microservice.users.model;

import com.fasterxml.jackson.annotation.*;
import com.wildadventures.microservice.users.enumeration.ProfilEnum;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the User model and it is related to the Users table
 * @author amarjane
 * @version 1.0
 */
//@JsonIgnoreProperties(ignoreUnknown = true)
//JsonInclude(JsonInclude.Include.NON_NULL)
//@Accessors(chain = true)
//@RequiredArgsConstructor
//@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
@Table(name = "users")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JoinColumn(name = "id_user", updatable=false, nullable=false)
    private long id_user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ProfilEnum profil;

    //private long idProfil;

//    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST, targetEntity = Profil.class)
//    @JoinColumn(name = "id_profil", referencedColumnName = "id_profil", insertable = false, updatable = false)
//    private Profil profil;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "name")
    private String name;

    @NotNull
    @Size(min = 1, max = 50)
    @JoinColumn(name = "lastname")
    private String lastname;

    @NotNull
    @Size(min = 1, max = 100)
    @JoinColumn(name = "mail", unique = true)
    private String mail;

    @NotNull
    @Size(min = 1, max = 100)
    @JoinColumn(name = "password")
    private String password;

//, fetch = FetchType.EAGER,  cascade = CascadeType.ALL)//, orphanRemoval = true)// , mappedBy = "address", targetEntity = Address.class)
    //@Column(name = "addressList")//, insertable = false, updatable = false)
    //@JoinColumn(name = "id_user", referencedColumnName = "id_user",table = "address", insertable = false, updatable = false)

    //@JsonManagedReference
    @OneToMany( mappedBy = "id_user", fetch = FetchType.LAZY)//, cascade = CascadeType.DETACH)
    private List<Address> addressList;

}

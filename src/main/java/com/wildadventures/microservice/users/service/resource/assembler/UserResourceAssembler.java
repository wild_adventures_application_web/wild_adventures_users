package com.wildadventures.microservice.users.service.resource.assembler;

import com.wildadventures.microservice.users.model.User;
import com.wildadventures.microservice.users.service.controller.UserController;
import org.springframework.hateoas.*;
import org.springframework.stereotype.Component;


import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Component
public class UserResourceAssembler implements ResourceAssembler<User, Resource<User>> {

    /**
     * Converts the given entity into an {@link ResourceSupport}.
     *
     * @param entity
     * @return resource
     */
    @Override
    public Resource<User> toResource(User entity) {

        Resource<User> resource = new Resource<>(entity);
        addLinks(resource);

        return resource;
    }

    /**
     * Converts the given entity into an {@link ResourceSupport}.
     *
     * @param entity
     * @return resources
     */
    public Resources<Resource<User>> toResource(List<Resource<User>> entity) {

        Resources<Resource<User>> resources = new Resources<>(entity);
        addLinks(resources);

        return resources;
    }
    private void addLinks(Resource<User> resource) {
        resource.add(linkTo(UserController.class).slash(resource.getContent()).withSelfRel());
    }
    private void addLinks(Resources<Resource<User>> resources) {
        resources.add(linkTo(UserController.class).slash(resources.getContent()).withSelfRel());
    }
}

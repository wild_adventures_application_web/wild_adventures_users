package com.wildadventures.microservice.users.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class Exception for NOT_CONTENT
 * @author amarjane
 * @version 1.0
 */
@ResponseStatus(HttpStatus.NO_CONTENT)
public class ResourceNoContentException extends RuntimeException  {

    public ResourceNoContentException() {
        super();
    }

    public ResourceNoContentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNoContentException(String message) {
        super(message);
    }

    public ResourceNoContentException(Throwable cause) {
        super(cause);
    }

}

package com.wildadventures.microservice.users.service.controller;

import com.wildadventures.microservice.users.business.contract.AddressManager;
import com.wildadventures.microservice.users.business.contract.UserManager;
import com.wildadventures.microservice.users.enumeration.Strings;
import com.wildadventures.microservice.users.model.Address;
import com.wildadventures.microservice.users.model.User;
import com.wildadventures.microservice.users.service.exceptions.ResourceNotFoundException;
import com.wildadventures.microservice.users.service.resource.assembler.AddressResourceAssembler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller Restful AddressController
 * @author amarjane
 * @version 1.0
 */
@Api(description="Service pour les opérations CRUD sur les address")
@RestController
public class AddressController {

    private final UserManager userManager;
    private final AddressManager addressManager;
    private final AddressResourceAssembler addressResourceAssembler;
    private static final Logger logger = LogManager.getLogger(UserController.class);

    /**
     * Find all address with id user
     * @link /address/all/id/{id}
     * @return ResponseEntity<?>
     */
    @ApiOperation(value = "Récupère tout les addresses d'un utilisateur")
    @GetMapping(value = "/address/user/id/{id}")
    public ResponseEntity<List<Address>> readAllAddress(@PathVariable("id") Long id) {

        logger.info(Strings.SEARCH_USER_IN_PROGRESS.toString());

        if (null != userManager.findUserById(id)) {

            logger.info(Strings.SEARCH_ADDRESS_IN_PROGRESS.toString());

            List<Address> addresses = addressManager.findAddressByUser(id);

            logger.info(Strings.VERIFY_LIST_ALL_ADDRESS_NOT_NULL.toString());

            if (addresses == null || addresses.size() <= 0) {

                logger.error(Strings.EXCEPTION_NOT_FOUND_ALL_ADDRESS.toString());

                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND).body(null);
                        //.body(Strings.EXCEPTION_NOT_FOUND_ALL_ADDRESS.toString());
            }

//            logger.info(Strings.CREATE_RESOURCE_HATEOAS.toString());
//
//            List<Resource<Address>> resources = addresses
//                    .stream()
//                    .map(addressResourceAssembler::toResource)
//                    .collect(Collectors.toList());

            //Resources<Resource<Address>> resource = addressResourceAssembler.toResource(resources);

            logger.info(Strings.SUCCESS_FIND_ADDRESS.toString());

            return ResponseEntity.ok(addresses);
        }else {

            logger.error(Strings.EXCEPTION_NOT_FOUND_USER.toString());

            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND).body(null);
                    //.body(Strings.EXCEPTION_NOT_FOUND_USER.toString());
        }
    }

    /**
     * Search Address with id type Long
     * @link /address/id/{id}
     * @param id
     * @return ResponseEntity<?>
     */
    @ApiOperation(value = "Récupère une addresse grâce à son ID à condition que celui-ci existe")
    @GetMapping(value = "/address/id/{id}")
    public ResponseEntity<Address> readOneAddressById(@PathVariable("id") Long id){

        logger.info(Strings.SEARCH_ADDRESS_IN_PROGRESS.toString());

        Address address = addressManager.findAddressById(id);

        if(address == null) {

            logger.error(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());

            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND).body(address);
                    //.body(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());

        }

        Resource<Address> resource = addressResourceAssembler.toResource(address);

        logger.info(Strings.SUCCESS_FIND_ADDRESS.toString());

        return ResponseEntity
                .ok(address);

    }

    /**
     * Search Address with id type Long
     * @link /address/nameandid/{name}/{id}
     * @param name,id
     * @return ResponseEntity<?>
     */
    @ApiOperation(value = "Récupère un addresse grâce à son nom et son id à condition que celui-ci existe")
    @GetMapping(value = "/address/nameandid/{name}/{id}")
    public ResponseEntity<Address> readOneAddressByNameAndId(@PathVariable("name") String name, @PathVariable("id") Long id) {

        logger.info(Strings.SEARCH_ADDRESS_IN_PROGRESS.toString());

        Address address = addressManager.findAddressByNameAndId(id, name);

        if (address == null) {

            logger.error(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());

            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND).body(null);
                    //.body(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());

        }

        logger.info(Strings.CREATE_RESOURCE_HATEOAS.toString());

        Resource<Address> resource = addressResourceAssembler.toResource(address);

        logger.info(Strings.SUCCESS_FIND_ADDRESS.toString());

        return ResponseEntity
                .ok(address);
    }

    /**
     * Create Address with Profil type Object
     * @link /Address/
     * @param address
     * @return ResponseEntity<?>
     */
    @ApiOperation(value = "Création d'une addresse")
    @PostMapping(value = "/address/")
    public ResponseEntity<Address> createAddress(@RequestBody Address address) throws URISyntaxException {

        logger.info(Strings.VERIFY_BODY_ADDRESS_NOT_NULL_OR_EMPTY.toString());

        if (address == null || isNullOrEmpty(address.getName()) || address.getId_location() <= 0){

            logger.error(Strings.EXCEPTION_NO_CONTENT_BODY_ADDRESS.toString());
            return ResponseEntity
                    .badRequest().body(null);
                    //.body(Strings.EXCEPTION_NO_CONTENT_BODY_ADDRESS.toString());
        }

        logger.info(Strings.SUCCESS_CREATE_PROFIL.toString());

        Resource<Address> resource = addressResourceAssembler.toResource(addressManager.createAddress(address));

        return ResponseEntity
                .created(new URI(resource.getLink(Link.REL_SELF).getHref()))
                .body(resource.getContent());
    }


    /**
     * Update partial Address with Address type Object
     * @link /address/id/{id}
     * @param newAddress, id
     * @return ResponseEntity<?>
     */
    @PatchMapping("/address/id/{id}")
    public ResponseEntity<Address> updateAddress(@RequestBody Address newAddress, @PathVariable Long id) {

        logger.info(Strings.VERIFY_ADDRESS_IS_EXIST);

        Address updateAddress = addressManager.findAddressById(id);

        if (updateAddress == null
                || newAddress.getId_address() != updateAddress.getId_address()
                || !newAddress.getId_user().equals(updateAddress.getId_user())){
            logger.error(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());
            return ResponseEntity
                    .badRequest().body(null);
                    //.body(Strings.EXCEPTION_NOT_FOUND_ADDRESS.toString());
        }



        if (newAddress == null
                || isNullOrEmpty(newAddress.getName())
                || newAddress.getId_location() <= 0
                || newAddress.getId_user() <= 0){

            logger.error(Strings.EXCEPTION_NO_CONTENT_BODY_ADDRESS.toString());

            return ResponseEntity
                    .badRequest().body(null);
                    //.body(Strings.EXCEPTION_NO_CONTENT_BODY_ADDRESS.toString());
        }

        try {
            //addressManager.updateAddress(newAddress);
            Resource<Address> resource = addressResourceAssembler.toResource(newAddress);
            return ResponseEntity
                    .created(new URI(resource.getId().expand().getHref()))
                    .body(resource.getContent());
        }catch (URISyntaxException e){
            return ResponseEntity
                    .badRequest().body(null);
                    //.body(Strings.EXCEPTION_UNABLE_TO_UPDATE_ADDRESS.toString() + newAddress);
        }
    }

    /**
     * Delete Address with id
     * @link /address/id/{id}
     * @param id
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/address/id/{id}")
    public ResponseEntity<?> deleteAddress(@PathVariable Long id) {

        try{
            addressManager.deleteAddressById(id);
            return ResponseEntity.noContent().build();
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }

    }

    /**
     * Delete Address with id user
     * @link /address/iduser/{id}
     * @param id
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/address/iduser/{id}")
    public ResponseEntity<?> deleteAddressIdUser(@PathVariable Long id) {
        try{
            addressManager.deleteAddressByIdUser(id);
            return ResponseEntity.noContent().build();
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }

    }


    /**
     * Method private for control if string it's not null or empty
     * @param str
     * @return boolean
     */
    private static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    public AddressController(UserManager userManager, AddressManager addressManager, AddressResourceAssembler addressResourceAssembler) {
        this.userManager = userManager;
        this.addressManager = addressManager;
        this.addressResourceAssembler = addressResourceAssembler;
    }
}

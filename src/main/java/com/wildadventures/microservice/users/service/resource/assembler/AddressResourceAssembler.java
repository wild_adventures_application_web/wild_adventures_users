package com.wildadventures.microservice.users.service.resource.assembler;

import com.wildadventures.microservice.users.model.Address;
import com.wildadventures.microservice.users.service.controller.AddressController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Component
public class AddressResourceAssembler implements ResourceAssembler<Address, Resource<Address>> {

    /**
     * Converts the given entity into an {@link ResourceSupport}.
     *
     * @param entity
     * @return resource
     */
    @Override
    public Resource<Address> toResource(Address entity) {

        Resource<Address> resource = new Resource<>(entity);
        addLinks(resource);

        return resource;
    }

    /**
     * Converts the given entity into an {@link ResourceSupport}.
     *
     * @param entity
     * @return resources
     */
    public Resources<Resource<Address>> toResource(List<Resource<Address>> entity) {

        Resources<Resource<Address>> resources = new Resources<>(entity);
        addLinks(resources);

        return resources;
    }
    private void addLinks(Resource<Address> resource) {
        resource.add(linkTo(AddressController.class).slash(resource.getContent()).withSelfRel());
    }
    private void addLinks(Resources<Resource<Address>> resources) {
        resources.add(linkTo(AddressController.class).slash(resources.getContent()).withSelfRel());
    }

}

package com.wildadventures.microservice.users.service.controller;

import com.wildadventures.microservice.users.beans.UserDetailsG;
import com.wildadventures.microservice.users.business.contract.UserManager;
import com.wildadventures.microservice.users.enumeration.*;
import com.wildadventures.microservice.users.model.User;
import com.wildadventures.microservice.users.service.exceptions.ResourceNotFoundException;
import com.wildadventures.microservice.users.service.resource.assembler.UserResourceAssembler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Controller Restful UserController
 * 
 * @author amarjane
 * @version 1.0
 */
@Api(description = "Service pour les opérations CRUD sur les utilisateurs")
@RestController
public class UserController {

	private final UserManager userManager;
	private final UserResourceAssembler userResourceAssembler;
	private static final Logger logger = LogManager.getLogger(UserController.class);

	/**
	 * Find all users
	 * 
	 * @link /users/
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Récupère tout les utilisateurs")
	@GetMapping(value = "/users/all")
	public ResponseEntity<List<User>> readAllUsers() {

		logger.info(Strings.SEARCH_USER_IN_PROGRESS.toString());

		List<User> users = userManager.findAllUsers();

		logger.info(Strings.VERIFY_LIST_ALL_USER_NOT_NULL.toString());

		if (users == null) {

			logger.error(Strings.EXCEPTION_NOT_FOUND_ALL_USERS.toString());

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			// .body(Strings.EXCEPTION_NOT_FOUND_ALL_USERS.toString());
		}

		// List<Resource<User>> resources = users
		// .stream()
		// .map(userResourceAssembler::toResource)
		// .collect(Collectors.toList());

		// Resources<Resource<User>> resource =
		// userResourceAssembler.toResource(resources);

		logger.info(Strings.SUCCESS_FIND_USERS.toString());

		return ResponseEntity.ok(users);

	}

	/**
	 * Search User with id type Long
	 * 
	 * @link /users/id/{id}
	 * @param id
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Récupère un utilisateur grâce à son ID à condition que celui-ci existe")
	@GetMapping(value = "/users/id/{id}")
	public ResponseEntity<User> readOneUsers(@PathVariable("id") Long id) {

		logger.info(Strings.SEARCH_USER_IN_PROGRESS.toString());

		User user = userManager.findUserById(id);

		if (user == null) {

			logger.error(Strings.EXCEPTION_NOT_FOUND_USER.toString());

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			// .body(Strings.EXCEPTION_NOT_FOUND_USER.toString());

		}

		// Resource<User> resource = userResourceAssembler.toResource(user);

		logger.info(Strings.SUCCESS_FIND_USER.toString());

		return ResponseEntity.ok(user);

	}

	/**
	 * Search User with mail type String
	 * 
	 * @link /users/mail/{mail}
	 * @param mail
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Récupère un utilisateur grâce à son mail à condition que celui-ci existe")
	@GetMapping(value = "/users/mail/{mail}")
	public ResponseEntity<UserDetailsG> readOneUsersByMail(@PathVariable("mail") String mail) {

		logger.info(Strings.SEARCH_USER_IN_PROGRESS.toString());

		User user = userManager.findUserByMail(mail);

		if (user == null) {

			logger.error(Strings.EXCEPTION_NOT_FOUND_USER.toString());

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			// .body(Strings.EXCEPTION_NOT_FOUND_USER.toString());

		}

		// Resource<User> resource = userResourceAssembler.toResource(user);

		logger.info(Strings.SUCCESS_FIND_USER.toString());

		UserDetailsG userDetails = new UserDetailsG();
		userDetails.setMail(user.getMail());
		userDetails.setPassword(user.getPassword());
		userDetails.setProfil(user.getProfil());

		return ResponseEntity.ok(userDetails);

	}

	@ApiOperation(value = "Récupère un utilisateur grâce à son mail à condition que celui-ci existe")
	@GetMapping(value = "/users/mailComplet/{mail}")
	public ResponseEntity<User> readOneUsersByMailComplet(@PathVariable("mail") String mail) {

		logger.info(Strings.SEARCH_USER_IN_PROGRESS.toString());

		User user = userManager.findUserByMail(mail);

		if (user == null) {

			logger.error(Strings.EXCEPTION_NOT_FOUND_USER.toString());

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			// .body(Strings.EXCEPTION_NOT_FOUND_USER.toString());

		}

		// Resource<User> resource = userResourceAssembler.toResource(user);

		logger.info(Strings.SUCCESS_FIND_USER.toString());

		return ResponseEntity.ok(user);

	}

	/**
	 * Create User with User type Object
	 * 
	 * @link /users/
	 * @param user
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Création d'un utilisateur")
	@PostMapping(value = "/users")
	public ResponseEntity<User> createUsers(@RequestBody User user) throws URISyntaxException {

		logger.info(Strings.VERIFY_BODY_USER_NOT_NULL_OR_EMPTY.toString());

		if (user == null || isNullOrEmpty(user.getName()) || isNullOrEmpty(user.getLastname())
				|| isNullOrEmpty(user.getMail()) || isNullOrEmpty(user.getPassword()) || user.getProfil() == null) {

			logger.error(Strings.EXCEPTION_NO_CONTENT_BODY_USER.toString());
			return ResponseEntity.badRequest().body(null);
			// .body(Strings.EXCEPTION_NO_CONTENT_BODY_USER.toString());
		}

		/*
		 * logger.info(Strings.VERIFY_USER_IS_NOT_EXIST.toString());
		 * 
		 * Profil profil = profilManager.findProfilById(user.getIdProfil());
		 * 
		 * if (profil == null){
		 * logger.error(Strings.EXCEPTION_NOT_FOUND_PROFIL.toString()); return
		 * ResponseEntity
		 * .badRequest().body(Strings.EXCEPTION_NOT_FOUND_PROFIL.toString()); }else {
		 * user.setProfil(profil); }
		 */
		logger.info(Strings.SUCCESS_CREATE_USER.toString());

		Resource<User> resource = userResourceAssembler.toResource(userManager.createUser(user));

		return ResponseEntity.created(new URI(resource.getLink(Link.REL_SELF).getHref())).body(resource.getContent());
	}

	/**
	 * Update partial User with User type Object
	 * 
	 * @link /users/id/{id}
	 * @param newUser,
	 *            id
	 * @return ResponseEntity<?>
	 */
	@PatchMapping("/users/id/{id}")
	public ResponseEntity<User> updateUsers(@RequestBody User newUser, @PathVariable Long id) {

		logger.info(Strings.VERIFY_USER_IS_EXIST);

		User updateUser = userManager.findUserById(id);

		if (updateUser == null || newUser.getId_user() != updateUser.getId_user()) {
			logger.error(Strings.EXCEPTION_NOT_FOUND_USER.toString());
			return ResponseEntity.badRequest().body(null);
			// .body(Strings.EXCEPTION_NOT_FOUND_USER.toString());
		}

		if (newUser == null || isNullOrEmpty(newUser.getName()) || isNullOrEmpty(newUser.getLastname())
				|| isNullOrEmpty(newUser.getMail()) || isNullOrEmpty(newUser.getPassword())
				|| newUser.getProfil() == null) {

			logger.error(Strings.EXCEPTION_NO_CONTENT_BODY_USER.toString());

			return ResponseEntity.badRequest().body(null);
			// .body(Strings.EXCEPTION_NO_CONTENT_BODY_USER.toString());
		}

		try {
			userManager.updateUser(newUser);
			Resource<User> resource = userResourceAssembler.toResource(newUser);
			return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(newUser);
		} catch (URISyntaxException e) {
			return ResponseEntity.badRequest().body(null);
			// .body(Strings.EXCEPTION_UNABLE_TO_UPDATE_USER.toString() + newUser);
		}
	}

	/**
	 * Delete User with id
	 * 
	 * @link /users/id/{id}
	 * @param id
	 * @return ResponseEntity<?>
	 */
	@DeleteMapping("/users/id/{id}")
	public ResponseEntity<?> deleteUsers(@PathVariable Long id) {

		try {
			userManager.deleteUser(id);
			return ResponseEntity.noContent().build();
		} catch (ResourceNotFoundException e) {
			return ResponseEntity.notFound().build();
		}

	}

	/**
	 * Method private for control if string it's not null or empty
	 * 
	 * @param str
	 * @return boolean
	 */
	private static boolean isNullOrEmpty(String str) {
		return str == null || str.trim().isEmpty();
	}

	UserController(UserManager userManager, UserResourceAssembler userResourceAssembler) {
		this.userManager = userManager;
		this.userResourceAssembler = userResourceAssembler;
	}
}

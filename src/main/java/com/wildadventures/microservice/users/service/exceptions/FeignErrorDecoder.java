package com.wildadventures.microservice.users.service.exceptions;

import feign.Response;
import feign.codec.ErrorDecoder;

/**
 * Class FeignEroorDecoder, it's for decode error exception > 2XX HTTP CODE
 * @see ErrorDecoder
 * @author amarjane
 * @version 1.0
 */
public class FeignErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder defaultErrorDecoder = new Default();

    @Override
    public Exception decode(String invoke, Response reponse) {

        if(reponse.status() == 404 ) {
            return new ResourceNotFoundException(
                    "Not Found"
            );
        }
        if(reponse.status() == 400 ) {
            return new ResourceNotFoundException(
                    "Bad Request"
            );
        }

        return defaultErrorDecoder.decode(invoke, reponse);
    }
}

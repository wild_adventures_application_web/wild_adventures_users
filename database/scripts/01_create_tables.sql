CREATE SEQUENCE public.users_id_user_seq;

CREATE TABLE public.users (
                id_user INTEGER NOT NULL DEFAULT nextval('public.users_id_user_seq'),
                mail VARCHAR(100) NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                profil VARCHAR NOT NULL,
                name VARCHAR(50) NOT NULL,
                password VARCHAR(100) NOT NULL,
                CONSTRAINT users_pk PRIMARY KEY (id_user)
);


ALTER SEQUENCE public.users_id_user_seq OWNED BY public.users.id_user;

CREATE SEQUENCE public.address_id_address_seq;

CREATE TABLE public.address (
                id_address INTEGER NOT NULL DEFAULT nextval('public.address_id_address_seq'),
                id_user INTEGER NOT NULL,
                name_address VARCHAR(50),
                city VARCHAR(50),
                country VARCHAR(50),
                postalcode VARCHAR(50),
                portal_code VARCHAR(50),
                door VARCHAR(50),
                id_location INTEGER NOT NULL,
                name VARCHAR(50) NOT NULL,
                stage VARCHAR(50),
                CONSTRAINT address_pk PRIMARY KEY (id_address)
);


ALTER SEQUENCE public.address_id_address_seq OWNED BY public.address.id_address;

ALTER TABLE public.address ADD CONSTRAINT user_address_fk
FOREIGN KEY (id_user)
REFERENCES public.users (id_user)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
